import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MenuIconComponent } from './header/menu-icon/menu-icon.component';
import { MenuModalComponent } from './header/menu-modal/menu-modal.component';
import { ContainerComponent } from './container/container.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MenuIconComponent,
    MenuModalComponent,
    ContainerComponent,
    FooterComponent,
  ],
  exports: [HeaderComponent, ContainerComponent, FooterComponent],
  imports: [CommonModule],
})
export class LayoutModule {}
