import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MenuModalService {
  private isOpenSubject = new BehaviorSubject<boolean>(false);

  isOpen$ = this.isOpenSubject.asObservable();

  openModal() {
    this.isOpenSubject.next(true);
  }

  toggleModal() {
    this.isOpenSubject.next(!this.isOpenSubject.value);
  }

  closeModal() {
    this.isOpenSubject.next(false);
  }
}
