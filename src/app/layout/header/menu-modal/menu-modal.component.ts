import { Component, Input } from '@angular/core';
import { MenuModalService } from './menu-modal.service';

@Component({
  selector: 'app-menu-modal',
  templateUrl: './menu-modal.component.html',
  styleUrls: ['./menu-modal.component.css'],
})
export class MenuModalComponent {
  @Input() isOpen = false;

  constructor(private modalService: MenuModalService) {
    this.modalService.isOpen$.subscribe((isOpen) => {
      this.isOpen = isOpen;
    });
  }

  close() {
    this.modalService.closeModal();
  }
}
