import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MenuModalService } from './menu-modal/menu-modal.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  isMenuOpen = false;
  private subscription: Subscription | null = null;

  constructor(private modalService: MenuModalService) {}

  ngOnInit() {
    this.subscription = this.modalService.isOpen$.subscribe((isOpen) => {
      this.isMenuOpen = isOpen;
    });
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  openModal() {
    this.modalService.openModal();
  }

  closeModal() {
    this.modalService.closeModal();
  }

  toggleModal() {
    this.modalService.toggleModal();
  }
}
