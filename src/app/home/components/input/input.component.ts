import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
})
export class InputComponent {
  formControl: FormControl = new FormControl('');
  @Output() inputValueChanged = new EventEmitter<string>();
  @Output() inputValidationStatus = new EventEmitter<boolean>();

  variant: 'error' | 'default' | 'success' | 'focus' = 'default';
  placeholderText: string = 'Enter you link here';
  errorText: string = '';

  onFocus() {
    if (this.variant !== 'error') {
      this.variant = 'focus';
    }
  }

  onBlur() {
    if (this.variant === 'focus') {
      this.variant = 'default';

      const regex = /\b\w{3,}\.\w{2,}\b/;
      if (!this.formControl.value || !regex.test(this.formControl.value)) {
        this.variant = 'error';
        this.errorText = 'Please add a link';
        this.inputValidationStatus.emit(false);
      } else {
        this.variant = 'success';
        this.errorText = '';
        this.inputValidationStatus.emit(true);
      }
    }
  }

  onInput(event: Event) {
    this.variant = 'focus';
    this.errorText = '';
    const target = event.target as HTMLInputElement;
    this.formControl.setValue(target.value);
    this.inputValueChanged.emit(target.value);
  }
}
