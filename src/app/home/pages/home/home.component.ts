import { Component, OnInit, ViewChild } from '@angular/core';
import { UrlShortenerService } from '../../services/url-shortener.service';
import { Link } from '../../models/link';
import { StorageService } from 'src/app/storage.service';
import { InputComponent } from '../../components/input/input.component';
import { ShortenResponse } from '../../models/shorten-response';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  longUrl = '';
  loading = false;
  isValid = false;
  links: Link[] = [];
  lastCopiedIndex: number | null = null;
  @ViewChild(InputComponent) appInput: InputComponent | undefined;

  constructor(
    private urlShortenerService: UrlShortenerService,
    private storageService: StorageService,
  ) {}

  ngOnInit(): void {
    this.loadStoredLinks();
  }

  private loadStoredLinks() {
    const storedLinks = this.storageService.get('links');
    if (storedLinks) {
      this.links = JSON.parse(storedLinks);
    }
  }

  onSubmit(event: Event) {
    event.preventDefault();
    if (!this.isValid) return;

    this.loading = true;
    this.urlShortenerService.shortenUrl(this.longUrl).subscribe(
      (response) => this.handleShortenUrlResponse(response),
      (error) => this.handleShortenUrlError(error),
    );
  }

  private handleShortenUrlResponse(response: ShortenResponse) {
    this.loading = false;
    if (response.ok) {
      const newLink: Link = {
        actualLink: this.longUrl,
        shortLink: response.result.short_link,
      };
      this.links = [...this.links, newLink];
      this.storageService.set('links', JSON.stringify(this.links));
    } else {
      console.error('Error shortening URL:', response);
    }
  }

  private handleShortenUrlError(error: Error) {
    this.loading = false;
    alert(error.message);
  }

  copyLink(index: number) {
    this.lastCopiedIndex = index;
    navigator.clipboard.writeText(this.links[index].shortLink);
  }

  onInputValueChanged(newValue: string) {
    this.longUrl = newValue;
  }

  onInputValidationStatusChanged(isValid: boolean) {
    this.isValid = isValid;
  }
}
