import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './pages/home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule } from '../layout/layout.module';
import { HeroComponent } from './components/hero/hero.component';
import { InputComponent } from './components/input/input.component';
import { CtaComponent } from './components/cta/cta.component';
import { WhyComponent } from './components/why/why.component';
import { HttpClientModule } from '@angular/common/http';
import { SpinnerLoadingComponent } from './components/spinner-loading/spinner-loading.component';

const routes: Routes = [{ path: '', component: HomeComponent }];

@NgModule({
  declarations: [HomeComponent, HeroComponent, InputComponent, CtaComponent, WhyComponent, SpinnerLoadingComponent],
  imports: [CommonModule, RouterModule.forChild(routes), LayoutModule, HttpClientModule],
  exports: [RouterModule],
})
export class HomeModule {}
