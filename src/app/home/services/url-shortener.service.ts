import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ShortenResponse } from '../models/shorten-response';

@Injectable({
  providedIn: 'root',
})
export class UrlShortenerService {
  private apiUrl = 'https://api.shrtco.de/v2/shorten';

  constructor(private http: HttpClient) {}

  shortenUrl(longUrl: string): Observable<ShortenResponse> {
    const params = { url: longUrl };
    return this.http.get<ShortenResponse>(this.apiUrl, { params });
  }
}
