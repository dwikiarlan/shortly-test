export interface Link {
  actualLink: string;
  shortLink: string;
}
