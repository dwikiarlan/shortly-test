/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/app/**/*.html'],
  darkMode: 'media',
  theme: {
    extend: {
      colors: {
        primary: '#2DD4BF',
        gray: {
          800: '#1F2937',
          500: '#6B7280',
        },
        red: {
          500: '#EF4444',
        },
        green: {
          500: '#10B981',
        },
        blue: {
          500: '#3B82F6',
        },
        purple: {
          800: '#6B21A8',
        },
        teal: {
          500: '#14B8A6',
          400: '#2DD4BF',
        },
        'true-gray': {
          800: '#262626',
        },
      },
      fontFamily: {
        sans: ['Poppins', 'sans'],
      },
      backgroundImage: {
        'shorten-mobile': "url('/assets/images/bg-shorten-mobile.svg')",
        'shorten-desktop': "url('/assets/images/bg-shorten-desktop.svg')",
        'boost-mobile': "url('/assets/images/bg-boost-mobile.svg')",
        'boost-desktop': "url('/assets/images/bg-boost-desktop.svg')",
        'icon-brand-recognition': "url('/assets/images/icon-brand-recognition.svg')",
        'icon-detailed-records': "url('/assets/images/icon-detailed-records.svg')",
        'icon-facebook': "url('/assets/images/icon-facebook.svg')",
        'icon-fully-customizable': "url('/assets/images/icon-fully-customizable.svg')",
        'icon-instagram': "url('/assets/images/icon-instagram.svg')",
        'icon-pinterest': "url('/assets/images/icon-pinterest.svg')",
        'icon-twitter': "url('/assets/images/icon-twitter.svg')",
        'illustration-working': "url('/assets/images/illustration-working.svg')",
      },
    },
  },
  variants: {},
  plugins: [],
};
